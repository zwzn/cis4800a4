package le

import "fmt"

type Plane struct {
	Point  *Vector3
	Normal *Vector3
}

func NewPlaneFromPoints(v1, v2, v3 *Vector3) *Plane {
	p := Plane{}
	p.Point = v1
	p.Normal = v1.Subtract(v2).Cross(v1.Subtract(v3))

	return &p
}

func (p *Plane) Intersection(l *Line) *Vector3 {
	if l.Direction.Inner(p.Normal) == 0 {
		return nil
	}

	d := p.Point.Subtract(l.Point).Inner(p.Normal) / l.Direction.Inner(p.Normal)
	return l.GetPoint(d)
}

func (p *Plane) String() string {
	return fmt.Sprintf("Point: %v\nNormal: %v", p.Point, p.Normal)
}
