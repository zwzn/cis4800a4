package le

import (
	"fmt"
	"math"

	"bitbucket.org/zwzn/cis4800a4/matrix"
)

type Vector3 struct {
	X float64
	Y float64
	Z float64
}

var Zero3 *Vector3 = &Vector3{0, 0, 0}

func Vec3(x, y, z float64) *Vector3 {
	return &Vector3{x, y, z}
}

func NewDirectionVector3(yaw, pitch float64) *Vector3 {
	return Vec3(0, 0, 1) //.RotateX(pitch).RotateY(yaw)
}

func Vec3FromMatrix(m *matrix.Matrix) *Vector3 {
	return &Vector3{m.Get(0, 0), m.Get(1, 0), m.Get(2, 0)}
}

func (v *Vector3) ToPosMatrix() *matrix.Matrix {
	return matrix.NewMatrix(4, 1, v.X, v.Y, v.Z, 1)
}

func (v *Vector3) ToDirMatrix() *matrix.Matrix {
	return matrix.NewMatrix(4, 1, v.X, v.Y, v.Z, 0)
}

func (v *Vector3) Transform(m *matrix.Matrix) (*Vector3, error) {
	vec, err := m.Multiply(v.ToPosMatrix())
	if err != nil {
		return nil, err
	}
	return Vec3FromMatrix(vec), nil
}

func (v *Vector3) Clone() *Vector3 {
	return &Vector3{
		v.X,
		v.Y,
		v.Z,
	}
}

func (v1 *Vector3) Add(v2 *Vector3) *Vector3 {
	return &Vector3{
		v1.X + v2.X,
		v1.Y + v2.Y,
		v1.Z + v2.Z,
	}
}
func (v1 *Vector3) Subtract(v2 *Vector3) *Vector3 {
	return &Vector3{
		v1.X - v2.X,
		v1.Y - v2.Y,
		v1.Z - v2.Z,
	}
}
func (v *Vector3) Negative() *Vector3 {
	return Zero3.Subtract(v)
}

func (v *Vector3) Scalar(a float64) *Vector3 {
	return &Vector3{
		a * v.X,
		a * v.Y,
		a * v.Z,
	}
}

func (this *Vector3) Inner(v *Vector3) float64 {
	return this.X*v.X + this.Y*v.Y + this.Z*v.Z
}

func (v1 *Vector3) Cross(v2 *Vector3) *Vector3 {

	return Vec3(
		v1.Y*v2.Z-v1.Z*v2.Y,
		v1.Z*v2.X-v1.X*v2.Z,
		v1.X*v2.Y-v1.Y*v2.X,
	)
}
func (this *Vector3) Magnitude() float64 {
	return math.Sqrt(this.X*this.X + this.Y*this.Y + this.Z*this.Z)
}

func (this *Vector3) Distance(v *Vector3) float64 {
	return math.Sqrt(math.Pow(math.Sqrt(math.Pow(this.X-v.X, 2)+math.Pow(this.Y-v.Y, 2)), 2) + math.Pow(this.Z-v.Z, 2))
}

func (this *Vector3) Angle(v *Vector3) float64 {
	// log.Fatal(this.Inner(v))
	return math.Acos(this.Inner(v) / (this.Magnitude() * v.Magnitude()))
}

func (v *Vector3) ToUnit() *Vector3 {
	mag := math.Abs(v.X) + math.Abs(v.Y) + math.Abs(v.Z)
	a := Vec3(
		v.X/mag,
		v.Y/mag,
		v.Z/mag)
	return a
}

func (v *Vector3) String() string {
	return fmt.Sprintf("(%.2f, %.2f, %.2f)", v.X, v.Y, v.Z)
}
