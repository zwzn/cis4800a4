package object

import "image/color"

type Light struct {
	Colour    color.RGBA
	Direction *Vector3
}

func NewLight(pos *Vector3, colour color.RGBA) *Light {
	return &Light{colour, pos}
}
