package object

import (
	"math"

	"bitbucket.org/zwzn/cis4800a4/matrix"
)

func NewTorus(sections int, hole float64) *Mesh {
	var m *Mesh
	m = newMesh()
	dps := math.Pi * 2 / float64(sections)
	for theta := 0.0; theta < math.Pi*2; theta += dps { // angle around the big loop
		for phi := 0.0; phi < math.Pi*2; phi += dps { // angle around the little loop

			p1 := tsToPoint(hole, phi, theta)
			p2 := tsToPoint(hole, phi, theta+dps)
			p3 := tsToPoint(hole, phi+dps, theta)
			p4 := tsToPoint(hole, phi+dps, theta+dps)

			n1 := tsToNorm(phi, theta)
			n2 := tsToNorm(phi, theta+dps)
			n3 := tsToNorm(phi+dps, theta)
			n4 := tsToNorm(phi+dps, theta+dps)

			m.addTriangle(NewTriangle(p1, p2, p3, n1, n2, n3))
			m.addTriangle(NewTriangle(p2, p3, p4, n2, n3, n4))
		}
	}
	return m
}

func tsToPoint(hole, phi, theta float64) *Vector3 {
	r := (2.0 - hole) / 4.0 // radius of the tube
	R := 1 - r              // distance from the center of the tube to the center of the torus

	t := matrix.Identity(4)

	t, _ = t.Multiply(matrix.Rotate(0, 0, theta))
	t, _ = t.Multiply(matrix.Translate(0, R, 0))

	t, _ = t.Multiply(matrix.Rotate(phi, 0, 0))
	t, _ = t.Multiply(matrix.Translate(0, r, 0))

	v, _ := Zero3.Transform(t)
	// fmt.Printf("(%0.2f, %0.2f)\n", r, R)

	return v
}
func tsToNorm(phi, theta float64) *Vector3 {

	t := matrix.Identity(4)

	t, _ = t.Multiply(matrix.Rotate(0, 0, theta))

	t, _ = t.Multiply(matrix.Rotate(phi, 0, 0))
	t, _ = t.Multiply(matrix.Translate(0, 1, 0))

	v, _ := Zero3.Transform(t)
	// fmt.Printf("(%0.2f, %0.2f)\n", r, R)

	return v.ToUnit()
}
