package object

import (
	"image/color"
	"math"

	"bitbucket.org/zwzn/cis4800a4/matrix"
)

const (
	PARALLEL    = iota
	PERSPECTIVE = iota
)

type Camera struct {
	position   *Vector3
	angle      *Vector3
	NearPlane  float64
	FarPlane   float64
	name       string
	projection int
	Ambient    color.RGBA
}

func NewCamera(pos *Vector3, angle *Vector3, fov float64, name string, projection int, ambient color.RGBA) *Camera {
	// I force h to be 1
	d := 1 / math.Tan(fov/2)
	// println("d: ", fov)
	return &Camera{pos, angle, d, 100, name, projection, ambient}
}

func (c *Camera) GetTransformations() (*matrix.Matrix, error) {

	m := matrix.Identity(4)

	m, err := matrix.Translate(
		-c.position.X,
		-c.position.Y,
		-c.position.Z).Multiply(m)
	if err != nil {
		return nil, err
	}

	m, err = matrix.Rotate(
		-c.angle.X,
		-c.angle.Y,
		-c.angle.Z).Multiply(m)
	if err != nil {
		return nil, err
	}

	return m, nil
}

func (c *Camera) GetPerspectiveTransform() (*matrix.Matrix, error) {
	var err error
	m := matrix.Identity(4)
	if c.projection == PERSPECTIVE {
		// I can ignore h because it is allways 1
		m, err = matrix.Mat4(
			c.NearPlane, 0, 0, 0,
			0, c.NearPlane, 0, 0,
			0, 0, c.FarPlane/(c.FarPlane-c.NearPlane), 1,
			0, 0, -(c.FarPlane*c.NearPlane)/(c.FarPlane-c.NearPlane), 0,
		).Multiply(m)
		if err != nil {
			return nil, err
		}
	}
	return m, nil
}
