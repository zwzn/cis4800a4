package object

import (
	"bitbucket.org/zwzn/cis4800a4/matrix"
)

func (m *Mesh) Transform(t *matrix.Matrix) error {
	var err error
	m.Transformation, err = t.Multiply(m.Transformation)
	return err
}
