package object

import (
	"fmt"
	"math"

	"bitbucket.org/zwzn/cis4800a4/matrix"
)

type Vector3 struct {
	X float64
	Y float64
	Z float64
}

type Vector2 struct {
	X float64
	Y float64
}

var Zero2 *Vector2 = &Vector2{0, 0}
var Zero3 *Vector3 = &Vector3{0, 0, 0}

func Vec2(x float64, y float64) *Vector2 {
	return &Vector2{x, y}
}

func (v1 *Vector2) Equals(v2 *Vector2) bool {
	return v1.X == v2.X && v1.Y == v2.Y
}

func (v *Vector2) Clone() *Vector2 {
	return &Vector2{
		v.X,
		v.Y,
	}
}

func (v1 *Vector2) Add(v2 *Vector2) *Vector2 {
	return &Vector2{
		v1.X + v2.X,
		v1.Y + v2.Y,
	}
}
func (v1 *Vector2) Subtract(v2 *Vector2) *Vector2 {
	return &Vector2{
		v1.X - v2.X,
		v1.Y - v2.Y,
	}
}

func (v *Vector2) Dot(a float64) *Vector2 {
	return &Vector2{
		a * v.X,
		a * v.Y,
	}
}

func (this *Vector2) Distance(v *Vector2) float64 {
	return math.Sqrt(math.Pow(this.X-v.X, 2) + math.Pow(this.Y-v.Y, 2))
}

func (v *Vector2) String() string {
	return fmt.Sprintf("(%.2f, %.2f)", v.X, v.Y)
}

func Vec3(x, y, z float64) *Vector3 {
	return &Vector3{x, y, z}
}

func (v1 *Vector3) Equals(v2 *Vector3) bool {
	return v1.X == v2.X && v1.Y == v2.Y && v1.Z == v2.Z
}

func NewDirectionVector3(yaw, pitch float64) *Vector3 {
	v, _ := Vec3(0, 0, 1).TransformDir(matrix.Rotate(pitch, yaw, 0))
	return v.ToUnit()
}

func Vec3FromMatrix(m *matrix.Matrix) *Vector3 {
	w := m.Get(3, 0)
	// fmt.Println(m)
	if w == 0 {
		return &Vector3{m.Get(0, 0), m.Get(1, 0), m.Get(2, 0)}
	}
	return &Vector3{m.Get(0, 0) / w, m.Get(1, 0) / w, m.Get(2, 0) / w}

}

func (v *Vector3) ToPosMatrix() *matrix.Matrix {
	return matrix.NewMatrix(4, 1, v.X, v.Y, v.Z, 1)
}

func (v *Vector3) ToDirMatrix() *matrix.Matrix {
	return matrix.NewMatrix(4, 1, v.X, v.Y, v.Z, 0)
}

func (v *Vector3) Transform(m *matrix.Matrix) (*Vector3, error) {
	vec, err := m.Multiply(v.ToPosMatrix())
	if err != nil {
		return nil, err
	}
	return Vec3FromMatrix(vec), nil
}
func (v *Vector3) TransformDir(m *matrix.Matrix) (*Vector3, error) {
	vec, err := m.Multiply(v.ToDirMatrix())
	if err != nil {
		return nil, err
	}
	return Vec3FromMatrix(vec).ToUnit(), nil
}

func (v *Vector3) Clone() *Vector3 {
	return &Vector3{
		v.X,
		v.Y,
		v.Z,
	}
}

func (v1 *Vector3) Add(v2 *Vector3) *Vector3 {
	return &Vector3{
		v1.X + v2.X,
		v1.Y + v2.Y,
		v1.Z + v2.Z,
	}
}
func (v1 *Vector3) Subtract(v2 *Vector3) *Vector3 {
	return &Vector3{
		v1.X - v2.X,
		v1.Y - v2.Y,
		v1.Z - v2.Z,
	}
}
func (v *Vector3) Negative() *Vector3 {
	return Zero3.Subtract(v)
}

func (v *Vector3) Scalar(a float64) *Vector3 {
	return &Vector3{
		a * v.X,
		a * v.Y,
		a * v.Z,
	}
}
func (this *Vector3) Inner(v *Vector3) float64 {
	return this.X*v.X + this.Y*v.Y + this.Z*v.Z
}

func (this *Vector3) Magnitude() float64 {
	return math.Sqrt(this.X*this.X + this.Y*this.Y + this.Z*this.Z)
}

func (this *Vector3) Distance(v *Vector3) float64 {
	return math.Sqrt(math.Pow(math.Sqrt(math.Pow(this.X-v.X, 2)+math.Pow(this.Y-v.Y, 2)), 2) + math.Pow(this.Z-v.Z, 2))
}

func (this *Vector3) Angle(v *Vector3) float64 {
	// log.Fatal(this.Inner(v))
	return math.Acos(this.Inner(v) / (this.Magnitude() * v.Magnitude()))
}

func mulArr(arr []float64, val float64) []float64 {
	ret := make([]float64, len(arr))
	for i, elem := range arr {
		ret[i] = elem * val
	}
	return ret
}
func divArr(arr []float64, val float64) []float64 {
	return mulArr(arr, 1/val)
}

func subArr(arr []float64, val []float64) []float64 {
	ret := make([]float64, len(arr))
	for i := 0; i < len(arr); i++ {
		ret[i] = arr[i] - val[i]
		// fmt.Printf("[%.2f = %.2f - %.2f]", ret[i], arr[i], val[i])
	}
	// println()
	return ret
}

// ToBasis returns a new vactor projected into a new basis b
func (v *Vector3) ToBasis(b *Basis) *Vector3 {
	x := []float64{b.I.X, b.J.X, b.K.X, 1.0, 0.0, 0.0}
	y := []float64{b.I.Y, b.J.Y, b.K.Y, 0.0, 1.0, 0.0}
	z := []float64{b.I.Z, b.J.Z, b.K.Z, 0.0, 0.0, 1.0}

	// nb := NewBasis(x[0], y[0], z[0], x[1], y[1], z[1], x[2], y[2], z[2])
	// ob := NewBasis(x[3], y[3], z[3], x[4], y[4], z[4], x[5], y[5], z[5])
	// fmt.Printf("TB1\n%v\n\n%v\n---------\n\n", nb, ob)

	// set ix to 1
	x = divArr(x, x[0])

	if y[0] != 0 {
		// set iy to 1
		y = divArr(y, y[0])
		// subtract x from y
		y = subArr(y, x)
	}

	if z[0] != 0 {
		// set iz to 1
		z = divArr(z, z[0])
		// sybtract x from z
		z = subArr(z, x)
	}

	// set jy to 1
	y = divArr(y, y[1])

	if z[1] != 0 {
		// set jz to 1
		z = divArr(z, z[1])
		// sybtract y from z
		z = subArr(z, y)
	}

	// set kz to 1
	z = divArr(z, z[2])

	//set ky to 0
	y = subArr(y, mulArr(z, y[2]))

	//set kx to 0
	x = subArr(x, mulArr(z, x[2]))

	//set jx to 0
	x = subArr(x, mulArr(y, x[1]))

	// nb = NewBasis(x[0], y[0], z[0], x[1], y[1], z[1], x[2], y[2], z[2])
	// ob = NewBasis(x[3], y[3], z[3], x[4], y[4], z[4], x[5], y[5], z[5])
	// fmt.Printf("TB3\n%v\n\n%v\n---------\n\n", nb, ob)

	// 	fmt.Printf(`
	// x = %.2f * %.2f + %.2f * %.2f + %.2f * %.2f
	// y = %.2f * %.2f + %.2f * %.2f + %.2f * %.2f
	// z = %.2f * %.2f + %.2f * %.2f + %.2f * %.2f
	// `, x[3], v.X, x[4], v.Y, x[5], v.Z,
	// 		y[3], v.X, y[4], v.Y, y[5], v.Z,
	// 		z[3], v.X, z[4], v.Y, z[5], v.Z)
	return Vec3(
		x[3]*v.X+x[4]*v.Y+x[5]*v.Z,
		y[3]*v.X+y[4]*v.Y+y[5]*v.Z,
		z[3]*v.X+z[4]*v.Y+z[5]*v.Z)

}

func (v *Vector3) ToUnit() *Vector3 {
	mag := v.Magnitude()
	a := Vec3(
		v.X/mag,
		v.Y/mag,
		v.Z/mag)
	return a
}

func (v *Vector3) String() string {
	return fmt.Sprintf("(%.2f, %.2f, %.2f)", v.X, v.Y, v.Z)
}
