package object

import (
	"fmt"
	"image/color"

	"bitbucket.org/zwzn/cis4800a4/matrix"
)

type Mesh struct {
	faces          []*Triangle
	Transformation *matrix.Matrix
	radius         float64
	Shine          float64
}

type Triangle struct {
	A       *Vector3
	B       *Vector3
	C       *Vector3
	NormalA *Vector3
	NormalB *Vector3
	NormalC *Vector3
	Colour  color.RGBA
	Mesh    *Mesh
}

func newMesh() *Mesh {
	m := &Mesh{}
	m.Transformation = matrix.Identity(4)
	return m
}

func (m *Mesh) addTriangle(t *Triangle) {
	t.Mesh = m
	m.faces = append(m.faces, t)
}

func (m *Mesh) GetFaces() []*Triangle {
	return m.faces
}

func (this *Mesh) SetColour(c color.RGBA) {
	for _, face := range this.GetFaces() {
		face.Colour = c
	}
}
func NewTriangle(a, b, c, na, nb, nc *Vector3) *Triangle {
	return &Triangle{a, b, c, na, nb, nc, color.RGBA{0, 0, 0, 0}, nil}
}

func (t *Triangle) Normal() *Vector3 {

	return Zero3.
		Add(t.NormalA).
		Add(t.NormalB).
		Add(t.NormalC).ToUnit()
}

func (t *Triangle) NormalFlat() *Vector3 {
	v := t.B.Subtract(t.A)
	w := t.C.Subtract(t.A)

	return Vec3(
		(v.Y*w.Z)-(v.Z*w.Y),
		(v.Z*w.X)-(v.X*w.Z),
		(v.X*w.Y)-(v.Y*w.X)).ToUnit()
}

func (t *Triangle) Distance(v *Vector3) float64 {
	return (t.A.Distance(v) + t.B.Distance(v) + t.C.Distance(v)) / 3
}

func (t *Triangle) Transform(m *matrix.Matrix) error {
	var err error
	t.A, err = t.A.Transform(m)
	if err != nil {
		return fmt.Errorf("error transforming vector A: %v\n", err)
	}
	t.B, err = t.B.Transform(m)
	if err != nil {
		return fmt.Errorf("error transforming vector B: %v\n", err)
	}
	t.C, err = t.C.Transform(m)
	if err != nil {
		return fmt.Errorf("error transforming vector C: %v\n", err)
	}

	t.NormalA, err = t.NormalA.TransformDir(m)
	if err != nil {
		return fmt.Errorf("error transforming vector A: %v\n", err)
	}
	t.NormalB, err = t.NormalB.TransformDir(m)
	if err != nil {
		return fmt.Errorf("error transforming vector B: %v\n", err)
	}
	t.NormalC, err = t.NormalC.TransformDir(m)
	if err != nil {
		return fmt.Errorf("error transforming vector C: %v\n", err)
	}
	return nil
}

func (t *Triangle) interpolate(pos *Vector2, a, b, c float64) float64 {

	xa := linearInterpolate(pos.Y, t.A.Y, t.B.Y, t.A.X, t.B.X)
	va := linearInterpolate(pos.Y, t.A.Y, t.B.Y, a, b)

	xb := linearInterpolate(pos.Y, t.A.Y, t.C.Y, t.A.X, t.C.X)
	vb := linearInterpolate(pos.Y, t.A.Y, t.C.Y, a, c)

	return linearInterpolate(pos.X, xa, xb, va, vb)
}

func linearInterpolate(value, oldMin, oldMax, newMin, newMax float64) float64 {
	return ((newMax-newMin)/(oldMax-oldMin))*(value-oldMin) + newMin
}

func (t *Triangle) InterpolateZ(pos *Vector2) float64 {
	return t.interpolate(pos, t.A.Z, t.B.Z, t.C.Z)
}
func (t *Triangle) InterpolateNormal(pos *Vector2) *Vector3 {
	// return t.NormalA.Add(t.NormalB).Add(t.NormalC).ToUnit()
	x := t.interpolate(pos, t.NormalA.X, t.NormalB.X, t.NormalC.X)
	y := t.interpolate(pos, t.NormalA.Y, t.NormalB.Y, t.NormalC.Y)
	z := t.interpolate(pos, t.NormalA.Z, t.NormalB.Z, t.NormalC.Z)
	return Vec3(x, y, z).ToUnit()
}
