package object

func NewCube(sections int) *Mesh {
	// TODO split into sub triangles
	var m *Mesh

	nTop := Vec3(0, 1, 0)
	nBottom := Vec3(0, -1, 0)
	nLeft := Vec3(-1, 0, 0)
	nRight := Vec3(1, 0, 0)
	nFront := Vec3(0, 0, 1)
	nBack := Vec3(0, 0, -1)

	m = newMesh()

	pps := 2.0 / float64(sections)

	// back
	for x := -1.0; x <= 1-pps*0.5; x += pps {
		for y := -1.0; y <= 1-pps*0.5; y += pps {

			p1 := Vec3(x, y, 1)
			p2 := Vec3(x, y+pps, 1)
			p3 := Vec3(x+pps, y, 1)
			p4 := Vec3(x+pps, y+pps, 1)

			m.addTriangle(NewTriangle(p1, p2, p3, nFront, nFront, nFront))
			m.addTriangle(NewTriangle(p2, p3, p4, nFront, nFront, nFront))
		}
	}

	// front
	for x := -1.0; x <= 1-pps*0.5; x += pps {
		for y := -1.0; y <= 1-pps*0.5; y += pps {

			p1 := Vec3(x, y, -1)
			p2 := Vec3(x, y+pps, -1)
			p3 := Vec3(x+pps, y, -1)
			p4 := Vec3(x+pps, y+pps, -1)

			m.addTriangle(NewTriangle(p1, p2, p3, nBack, nBack, nBack))
			m.addTriangle(NewTriangle(p2, p3, p4, nBack, nBack, nBack))
		}
	}

	// bottom
	for x := -1.0; x <= 1-pps*0.5; x += pps {
		for y := -1.0; y <= 1-pps*0.5; y += pps {

			p1 := Vec3(x, -1, y)
			p2 := Vec3(x, -1, y+pps)
			p3 := Vec3(x+pps, -1, y)
			p4 := Vec3(x+pps, -1, y+pps)

			m.addTriangle(NewTriangle(p1, p2, p3, nBottom, nBottom, nBottom))
			m.addTriangle(NewTriangle(p2, p3, p4, nBottom, nBottom, nBottom))
		}
	}

	// top
	for x := -1.0; x <= 1-pps*0.5; x += pps {
		for y := -1.0; y <= 1-pps*0.5; y += pps {

			p1 := Vec3(x, 1, y)
			p2 := Vec3(x, 1, y+pps)
			p3 := Vec3(x+pps, 1, y)
			p4 := Vec3(x+pps, 1, y+pps)

			m.addTriangle(NewTriangle(p1, p2, p3, nTop, nTop, nTop))
			m.addTriangle(NewTriangle(p2, p3, p4, nTop, nTop, nTop))
		}
	}

	// right
	for x := -1.0; x <= 1-pps*0.5; x += pps {
		for y := -1.0; y <= 1-pps*0.5; y += pps {

			p1 := Vec3(1, x, y)
			p2 := Vec3(1, x, y+pps)
			p3 := Vec3(1, x+pps, y)
			p4 := Vec3(1, x+pps, y+pps)

			m.addTriangle(NewTriangle(p1, p2, p3, nRight, nRight, nRight))
			m.addTriangle(NewTriangle(p2, p3, p4, nRight, nRight, nRight))
		}
	}

	// left
	for x := -1.0; x <= 1-pps*0.5; x += pps {
		for y := -1.0; y <= 1-pps*0.5; y += pps {

			p1 := Vec3(-1, x, y)
			p2 := Vec3(-1, x, y+pps)
			p3 := Vec3(-1, x+pps, y)
			p4 := Vec3(-1, x+pps, y+pps)

			m.addTriangle(NewTriangle(p1, p2, p3, nLeft, nLeft, nLeft))
			m.addTriangle(NewTriangle(p2, p3, p4, nLeft, nLeft, nLeft))
		}
	}
	return m
}
