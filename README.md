# CIS*4800 A2

Adam Bibby (0879972)

git repo [https://bitbucket.org/zwzn/cis4800a4](https://bitbucket.org/zwzn/cis4800a4)

## User Manual

### Installation

Install go as described here  [https://golang.org/doc/install](https://golang.org/doc/install).
Get the package by running `go get bitbucket.org/zwzn/cis4800a4`.
Install the package with `go install bitbucket.org/zwzn/cis4800a4`.

### Running

The executable will be in the go binary folder at `$GOPATH/bin/cis4800a4` or `$GOPATH/bin/cis4800a4.exe`.

The arguments are

- -if, string, an xobj file to be rendered
- -of, string, the image file to output (default "image.png")
- -size, int, The size of the output image (default 1000)
- -h, boolean, shows help page
- -aa, int, Anti Aliasing (default 1)

To render sphere.xobj to an image named sphere and an image size of 500px*500px you would use the command `cis4800a4 -if sphere.xobj -of sphere.png -size 500`

### Files

The files are the same as in a3 with a couple of additions

#### Light

To add lights to your scene you must add an amount of ambient light to the camera and or any amount of lights to the scene.

For a scene with an ambient light of 50 and 2 lights pitched up 75&deg;, 1 inline with the camera and red, the other rotated around the center 90&deg; and blue you would use the following camera and lights.
``` xml
<camera ambient="50">
    <spherical pitch="0" yaw="0" distance="5" />
</camera>

<light pitch="75" yaw="0" intensity="50" colour="#f00"/>
<light pitch="75" yaw="90" intensity="50" colour="#0f0"/>
```

#### Shine

Models now have a shine attribute that controls the shininess of the model. Values under 10 are quite matte, values from 10 to 30 are a semigloss and values of 30+ are quite glossy.

The following is an example of a matte red sphere.

``` xml
<mesh shape="sphere" resolution="50"  colour="#f00" shine="10" />
```

## Technical Discussion

### Interpolating Normals

To interpolate the normals of each face I used the same technique that I used to interpolate Z values, I just used it once for each of the X, Y, and Z components of the normal.

``` javascript
function InterpolateNormal(screenPos) {
    x := face.interpolate(screenPos, t.NormalA.X, t.NormalB.X, t.NormalC.X)
    y := face.interpolate(screenPos, t.NormalA.Y, t.NormalB.Y, t.NormalC.Y)
    z := face.interpolate(screenPos, t.NormalA.Z, t.NormalB.Z, t.NormalC.Z)
    return new Vector3(x, y, z).ToUnit()
}
```

A torus before and after adding normal interpolation

![torus no interpolation](https://adambibby.ca/cis4800/a4/torus1.png)
![torus with interpolation](https://adambibby.ca/cis4800/a4/torus2.png)

### Phong Reflection

When calculating reflections for each pixel I iterate through the lights 

``` javascript
for (light in object.lights) {
    lightReflectionVec := light.Direction.Scalar(-1).
        Add(norm.Scalar(2 * norm.Inner(light.Direction))).ToUnit()

    viewVec := worldPos.ToUnit()

    lv := new Vector3(
        float64(light.Colour.R), 
        float64(light.Colour.G), 
        float64(light.Colour.B)).
        Scalar(float64(light.Colour.A) / 255 / 255)

    shine := face.Shine

    diffuse := clamp(norm.Inner(light.Direction), 0, 1)
    specular := math.Pow(viewVec.Inner(lightReflectionVec), shine)

    kd := 1.0
    ks := clamp(shine/50.0, 0, 1)

    pixelColor = pixelColor.Add(cv.Scalar(kd).Scalar(diffuse).Add(lv.Scalar(ks).Scalar(specular)))
}
```

The folowing are examples of 0, 10, 30, and 100 shine on a torus

![shine 0](https://adambibby.ca/cis4800/a4/torus3.png)
![shine 10](https://adambibby.ca/cis4800/a4/torus4.png)
![shine 30](https://adambibby.ca/cis4800/a4/torus5.png)
![shine 100](https://adambibby.ca/cis4800/a4/torus6.png)