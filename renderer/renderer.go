package renderer

import (
	"fmt"
	"image"
	"image/color"
	"log"
	"math"

	"bitbucket.org/zwzn/cis4800a4/object"
	"bitbucket.org/zwzn/parallel"
	colorful "github.com/lucasb-eyer/go-colorful"
	pb "gopkg.in/cheggaaa/pb.v1"
)

var (
	Wireframe       = false
	BackFaceRemoval = true
	Clip            = true
	ZBuffer         = true
)

func RenderObject(o *object.Object, width int, height int, camName string) (*image.RGBA, error) {
	r := image.Rect(0, 0, width, height)
	img := image.NewRGBA(r)
	faces := o.GetFaces()
	var zMap [][]float64
	zMap = make([][]float64, width)
	for x := 0; x < width; x++ {
		zMap[x] = make([]float64, height)
		for y := 0; y < height; y++ {
			zMap[x][y] = 1
		}
	}
	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			c := color.RGBA{255, 255, 255, 255}
			// if y%100 == 0 || x%100 == 0 {
			// 	c = color.RGBA{0, 0, 0, 255}
			// }
			img.SetRGBA(
				x,
				y,
				c,
			)
		}
	}

	cam := o.GetCamera(camName)
	// fmt.Printf("%v\n", cam)
	numFaces := len(faces)
	bar := pb.StartNew(numFaces)

	minZ := 255.0
	maxZ := 0.0

	m1, err := cam.GetTransformations()
	if err != nil {
		return nil, err
	}
	m2, err := cam.GetPerspectiveTransform()
	if err != nil {
		return nil, err
	}

	fullTransform, err := m2.Multiply(m1)
	if err != nil {
		return nil, err
	}

	parallel.For(0, len(o.Lights), 1, func(i, thread, total int) {
		o.Lights[i].Direction, err = o.Lights[i].Direction.Transform(fullTransform)
		if err != nil {
			log.Fatalf("error in positioning lights: %v\n", err)
		}
		// o.Lights[i].Position, err = o.Lights[i].Position.Transform()
		// if err != nil {
		// 	log.Fatalf("error in positioning lights: %v\n", err)
		// }
	})
	parallel.For(0, numFaces, 1, func(i, thread, total int) {
		face := faces[i]

		bar.Increment()

		m, err := cam.GetTransformations()
		if err != nil {
			log.Fatalf("error in render: %v\n", err)
		}
		t, err := m.Multiply(face.Mesh.Transformation)
		if err != nil {
			log.Fatalf("error in render: %v\n", err)
		}

		err = face.Transform(t)
		if err != nil {
			log.Fatalf("error in render: %v\n", err)
		}

		// fmt.Printf("Normal: %v\n", face.Normal())
		if object.Vec3(0, 0, 1).Inner(face.Normal()) >= 0 && BackFaceRemoval {
			return
		}

		m, err = cam.GetPerspectiveTransform()
		if err != nil {
			log.Fatalf("error in render: %v\n", err)
		}
		err = face.Transform(m)
		if err != nil {
			log.Fatalf("error in render: %v\n", err)
		}

		pa, errA := project(face.A, width, height)
		pb, errB := project(face.B, width, height)
		pc, errC := project(face.C, width, height)

		if errA != nil && errB != nil && errC != nil {
			return
		} else if (errA != nil || errB != nil || errC != nil) && !Clip {
			return
		}

		if Wireframe {
			drawLine(img, pa, pb, face.Colour)
			drawLine(img, pa, pc, face.Colour)
			drawLine(img, pb, pc, face.Colour)
			return
		}

		m1 := lineToMap(calcLine(pa, pb))
		m2 := lineToMap(calcLine(pa, pc))
		m3 := lineToMap(calcLine(pb, pc))

		minX, maxX :=
			int(math.Min(pa.X, math.Min(pb.X, pc.X))),
			int(math.Max(pa.X, math.Max(pb.X, pc.X)))
		minY, maxY :=
			int(math.Min(pa.Y, math.Min(pb.Y, pc.Y))),
			int(math.Max(pa.Y, math.Max(pb.Y, pc.Y)))

		se := map[int][]int{}

		for y := minY; y <= maxY; y++ {
			rowSE := []int{}
			for x := minX; x <= maxX; x++ {
				if m1[y] == x || m2[y] == x || m3[y] == x {
					if len(rowSE) == 0 {
						rowSE = append(rowSE, x)
					} else if len(rowSE) == 1 {
						rowSE = append(rowSE, x)
					} else {
						rowSE[1] = x
					}
				}

			}
			if len(rowSE) == 1 {
				rowSE = append(rowSE, rowSE[0])
			}
			if len(rowSE) != 0 {
				se[y] = rowSE
			}
		}

		cv := object.Vec3(float64(face.Colour.R), float64(face.Colour.G), float64(face.Colour.B)).Scalar(1.0 / 255.0)

		aa := float64(cam.Ambient.A) / 255 / 255
		ar := float64(cam.Ambient.R) * aa
		ab := float64(cam.Ambient.B) * aa
		ag := float64(cam.Ambient.G) * aa

		// fmt.Println(ar)
		// fmt.Printf("%v\n", aa*255*255)
		// rc := color.RGBA{uint8(rand.Intn(255)), uint8(rand.Intn(255)), uint8(rand.Intn(255)), 255}
		for y, rowSE := range se {
			for x := rowSE[0]; x <= rowSE[1]; x++ {
				if x >= 0 && x < width && y >= 0 && y < height {

					pos := object.Vec2(float64(x), float64(y))
					uPos := unproject(pos, width, height)
					z := face.InterpolateZ(uPos)
					if z > 0 && z < 1 {
						uPos3 := object.Vec3(uPos.X, uPos.Y, z)
						norm := face.InterpolateNormal(uPos)
						if z < zMap[x][y] || !ZBuffer {
							zMap[x][y] = z
							minZ = math.Min(z, minZ)
							maxZ = math.Max(z, maxZ)
							c := cv.Add(object.Zero3)

							c.X *= ar
							c.Y *= ag
							c.Z *= ab

							shine := face.Mesh.Shine

							for _, light := range o.Lights {
								lightVec := light.Direction
								lightReflectionVec := lightVec.Scalar(-1).Add(norm.Scalar(2 * norm.Inner(lightVec))).ToUnit()
								viewVec := uPos3.ToUnit()

								la := float64(light.Colour.A) / 255 / 255
								lv := object.Vec3(float64(light.Colour.R), float64(light.Colour.G), float64(light.Colour.B)).Scalar(la)

								diffuse := clamp(norm.Inner(lightVec), 0, 1)
								specular := math.Pow(viewVec.Inner(lightReflectionVec), shine)
								// specular = 1
								kd := 1.0 // clamp((100.0-shine)/100.0, 0, 1)
								ks := clamp(shine/20.0, 0, 1)

								c = c.Add(
									cv.
										Scalar(kd).
										Scalar(diffuse).
										Add(
											lv.
												Scalar(ks).
												Scalar(specular),
										),
								)
							}

							img.SetRGBA(x, y, color.RGBA{
								uint8(clamp(c.X*255, 0, 255)),
								uint8(clamp(c.Y*255, 0, 255)),
								uint8(clamp(c.Z*255, 0, 255)),
								255,
							})
							// img.SetRGBA(x, y, color.RGBA{100, 0, uint8(z * 255), 255})
							// img.SetRGBA(x, y, rc)
						}

					}
				}
			}

		}

	})

	bar.FinishPrint("Finished render")
	fmt.Println()
	if false { // depth map
		for x := 0; x < width; x++ {
			for y := 0; y < height; y++ {
				if zMap[x][y] != 1 {
					// c := img.RGBAAt(x, y)
					z := linearInterpolate(zMap[x][y], minZ, maxZ, 0.0, 300.0)
					gc := colorful.Hsv(z, 0.50, 0.9)
					r, g, b := gc.RGB255()
					// c.R = uint8(z)
					// c.G = uint8(z)
					// c.B = uint8(z)
					img.SetRGBA(x, y, color.RGBA{r, g, b, 255})
				}
			}
		}
	}

	return img, nil
}
func clamp(val, min, max float64) float64 {
	return math.Max(min, math.Min(max, val))
}
func linearInterpolate(value, oldMin, oldMax, newMin, newMax float64) float64 {
	return ((newMax-newMin)/(oldMax-oldMin))*(value-oldMin) + newMin
}

func project(p *object.Vector3, width int, height int) (*object.Vector2, error) {
	w := float64(width - 1)
	h := float64(height - 1)

	newP := object.Vec2(p.X, p.Y). //p2d.
					Add(object.Vec2(1, 1)).
					Dot(math.Min(w, h) / 2)

	if p.Z < 0 || p.Z > 1 ||
		p.X < -1 || p.X > 1 ||
		p.Y < -1 || p.Y > 1 {
		return newP, fmt.Errorf("The point was behind the camera")
	}
	return newP, nil
}

func unproject(p *object.Vector2, width int, height int) *object.Vector2 {
	w := float64(width - 1)
	h := float64(height - 1)

	newP := p.
		Dot(2 / math.Min(w, h)).
		Subtract(object.Vec2(1, 1))
	return newP
}

func calcLine(p1 *object.Vector2, p2 *object.Vector2) []*object.Vector2 {
	if p1 == nil || p2 == nil {
		log.Fatalf("drawLine: One or both of the vectors are nil\n")
	}
	var sx, ex, sy, ey float64
	line := []*object.Vector2{}
	if math.Abs(p1.X-p2.X) > math.Abs(p1.Y-p2.Y) {
		if p1.X < p2.X {
			sx = float64(int(p1.X))
			sy = float64(int(p1.Y))
			ex = float64(int(p2.X))
			ey = float64(int(p2.Y))
		} else {
			sx = float64(int(p2.X))
			sy = float64(int(p2.Y))
			ex = float64(int(p1.X))
			ey = float64(int(p1.Y))
		}

		// fmt.Println("x", sx, sy, ex, ey)
		for x := sx; x <= ex; x++ {
			// println("test")
			y := sy + (x-sx)/(sx-ex)*(sy-ey)
			line = append(line, object.Vec2(x, y))
		}
	} else {
		if p1.Y < p2.Y {
			sx = float64(int(p1.X))
			sy = float64(int(p1.Y))
			ex = float64(int(p2.X))
			ey = float64(int(p2.Y))
		} else {
			sx = float64(int(p2.X))
			sy = float64(int(p2.Y))
			ex = float64(int(p1.X))
			ey = float64(int(p1.Y))
		}
		// fmt.Println("y", sx, sy, ex, ey)
		for y := sy; y <= ey; y++ {

			x := sx + (y-sy)/(sy-ey)*(sx-ex)
			// fmt.Println(x, y)
			line = append(line, object.Vec2(x, y))
		}
	}
	return line
}

func drawLine(img *image.RGBA, p1 *object.Vector2, p2 *object.Vector2, colour color.RGBA) {
	for _, p := range calcLine(p1, p2) {
		img.SetRGBA(int(p.X), int(p.Y), colour)
	}
}
func lineToMap(line []*object.Vector2) map[int]int {
	m := map[int]int{}
	for _, p := range line {
		m[int(p.Y)] = int(p.X)
	}
	return m
}

func drawPoint(img *image.RGBA, p *object.Vector2, colour color.RGBA) {
	img.SetRGBA(
		int(p.X),
		int(p.Y),
		colour,
	)
}
