#!/bin/bash

# Basic rendering
echo "samples/imgs/01-torus.png"
cis4800a4 -if samples/xobjs/torus.xobj -of samples/imgs/01-torus.png
echo "samples/imgs/02-warch.png"
cis4800a4 -if <(php samples/xobjs/watch.php)   -of samples/imgs/02-warch.png

# culling
echo ""
echo "----------- CULLING -----------"
echo ""
echo "renderer/renderer.go:100"
echo ""
echo "samples/imgs/03-tube-wireframe-backface.png"
cis4800a4 -if samples/xobjs/tube.xobj -wireframe -back-face -of samples/imgs/03-tube-wireframe-backface.png
echo "samples/imgs/04-tube-wireframe.png "
cis4800a4 -if samples/xobjs/tube.xobj -wireframe            -of samples/imgs/04-tube-wireframe.png 

# clipping
echo ""
echo "----------- CLIPPING -----------"
echo ""
echo "renderer/renderer.go:117"
echo "renderer/renderer.go:177"
echo ""
echo "samples/imgs/05-torus-no-clip.png"
cis4800a4 -if samples/xobjs/torus.xobj -c zoom -clip -of samples/imgs/05-torus-no-clip.png
echo "samples/imgs/06-torus-clip.png"
cis4800a4 -if samples/xobjs/torus.xobj -c zoom       -of samples/imgs/06-torus-clip.png

# rasterizing
echo ""
echo "----------- RASTER -----------"
echo ""
echo "renderer/renderer.go:130"
echo "renderer/renderer.go:175"
echo "renderer/renderer.go:223"
echo ""
echo "samples/imgs/07-cube-wf.png"
cis4800a4 -if samples/xobjs/cube.xobj -wireframe -of samples/imgs/07-cube-wf.png
echo "samples/imgs/08-cube-raster.png "
cis4800a4 -if samples/xobjs/cube.xobj            -of samples/imgs/08-cube-raster.png 

# hidden surface removal
echo ""
echo "----------- HIDDEN SURFACE REMOVAL -----------"
echo ""
echo "renderer/renderer.go:185"
echo ""
echo "samples/imgs/9-watch-wf.png"
cis4800a4 -if <(php samples/xobjs/watch.php) -zbuffer -of samples/imgs/9-watch-wf.png
echo "samples/imgs/10-watch-raster.png "
cis4800a4 -if <(php samples/xobjs/watch.php)          -of samples/imgs/10-watch-raster.png 

# phong
echo ""
echo "----------- PHONG SHADING -----------"
echo ""
echo "renderer/renderer.go:195"
echo ""
echo "samples/imgs/11-torus-no-light.png"
cis4800a4 -if samples/xobjs/torus-no-light.xobj -of samples/imgs/11-torus-no-light.png
echo "samples/imgs/12-torus.png"
cis4800a4 -if samples/xobjs/torus.xobj          -of samples/imgs/12-torus.png