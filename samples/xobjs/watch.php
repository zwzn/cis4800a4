<object>
    <camera nam="iso" ambient="25">
        <spherical pitch="60" yaw="20" distance="5" />        
    </camera>
    <camera name="top" ambient="25">
        <spherical pitch="90" yaw="1" distance="5" />        
    </camera>
    <camera name="right" ambient="25">
        <spherical pitch="0" yaw="90" distance="5" />        
    </camera>
    <camera name="zoom" ambient="25">
        <spherical pitch="60" yaw="20" distance="3" />       
    </camera>

    <light pitch="75" yaw="0" intensity="75" colour="#fff"/>
    
    <?php
        $ticks = "#f00";
        $case = "#e0e0e0";
        $face = "#333";
        // $face = "#0f0";

        $shine = 100;
    ?>

    <!-- case -->
    <mesh shape="tube" resolution="50" hole="1.7" colour="<?= $case ?>" shine="<?= $shine ?>">
        <transform type="scale" y="0.2"/>
    </mesh>
    <?php for ($i = 0; $i < 4; $i++) { ?>
        <mesh shape="cube" resolution="1" colour="<?= $case ?>" shine="<?= $shine ?>"> 
            <?php
                $a = 15;
                $b = 1.5;
            ?>
            <transform type="rotate" x="<?= -$a ?>"/>
            <transform type="scale"  z="<?= 1 / $b ?>"/>
            <transform type="rotate" x="<?= $a ?>"/>
            <transform type="scale"  z="<?= $b ?>"/>

            <transform type="scale" x="0.1" y="0.1" z="0.27"/>
            <transform type="rotate" x="-20"/>
            <transform type="translate" x="0.7" y="0.05" z="-0.86" />

            <?php if ($i % 2 == 0) { ?>
                <transform type="rotate" y="180"/>
            <?php } ?>
            <?php if ($i > 1) { ?>
                <transform type="scale"  z="-1"/>
            <?php } ?>

        </mesh>
    <?php } ?>

    <?php for ($i = 0; $i < 90; $i += 90 / 6) { ?>
        <mesh shape="cube" resolution="2" colour="<?= $ticks ?>" shine="<?= $shine ?>">        
            <transform type="rotate" y="<?= $i ?>" />  
            <transform type="scale" x="0.1" z="0.1" y="0.1"/>
            <transform type="rotate" z="90"/>
            <transform type="translate" x="1" />  
        </mesh>
    <?php } ?>
    

    <!-- face -->
    <mesh shape="cylinder" resolution="50" colour="<?= $face ?>" shine="<?= $shine ?>">
        <transform type="scale" x=".95" z="0.95" y="0.1"/>
        <transform type="translate" y="0.05" />
    </mesh>    
    <!-- 12 3 6 9 -->
    <?php for ($i = 0; $i < 360; $i += 90) { ?>
        <mesh shape="cube" resolution="1" colour="<?= $ticks ?>" shine="<?= $shine ?>"> 
            <transform type="scale" x="0.03" y="0.08" z="0.15"/>
            <!-- <transform type="rotate" x="-20"/> -->
            <transform type="translate" z="0.7"/>
            <transform type="rotate" y="<?= $i ?>"/>
        </mesh>
    <?php } ?>
    
    <!-- small ticks -->
    <?php for ($i = 0; $i < 360; $i += 30) { ?>
        <mesh shape="cube" resolution="1" colour="<?= $ticks ?>" shine="<?= $shine ?>"> 
            <transform type="scale" x="0.02" y="0.08" z="0.1"/>
            <!-- <transform type="rotate" x="-20"/> -->
            <transform type="translate" z="0.75"/>
            <transform type="rotate" y="<?= $i ?>"/>
        </mesh>
    <?php } ?>

    <!-- hands -->
    <mesh shape="cube" resolution="1" colour="<?= $case ?>" shine="<?= $shine ?>"> 
        <transform type="translate" z="1"/>
        <transform type="scale" x="0.03" y="0.02" z="0.3"/>
        <!-- <transform type="rotate" x="-20"/> -->
        <transform type="translate" y="-0.1"/>
        <transform type="rotate" y="-120"/>
    </mesh>
    <mesh shape="cube" resolution="1" colour="<?= $case ?>" shine="<?= $shine ?>"> 
        <transform type="translate" z="1"/>
        <transform type="scale" x="0.03" y="0.02" z="0.4"/>
        <!-- <transform type="rotate" x="-20"/> -->
        <transform type="translate" y="-0.1" />
        <transform type="rotate" y="-60"/>
    </mesh>    
    <mesh shape="cylinder" resolution="10" colour="<?= $case ?>" shine="<?= $shine ?>">
        <transform type="scale" x="0.06" y="0.15" z="0.06"/>
    </mesh>
    
</object>