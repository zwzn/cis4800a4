package main

import (
	"flag"
	"image"
	"image/png"
	"log"
	"os"

	"bitbucket.org/zwzn/cis4800a4/object"
	"bitbucket.org/zwzn/cis4800a4/renderer"
	"github.com/nfnt/resize"
)

func check(e error) {
	if e != nil {
		log.Fatalf("%v\n", e)
	}
}

func main() {

	inFile := flag.String("if", "", "an xobj file to be rendered")
	outFile := flag.String("of", "image.png", "the image file to output")
	size := flag.Int("size", 500, "The size of the output image")
	camera := flag.String("camera", "", "The name of the camera to use of empty for default")
	c := flag.String("c", "", "An alias for camera")
	aa := flag.Int("aa", 8, "Anti Aliasing")

	wf := flag.Bool("wireframe", false, "show the wireframe")
	bf := flag.Bool("back-face", false, "show back facing polygons")
	clip := flag.Bool("clip", false, "show back facing polygons")
	zbuf := flag.Bool("zbuffer", false, "ignore the zbuffer")

	flag.Parse()

	if *c != "" {
		camera = c
	}

	if *inFile == "" {
		log.Fatalf("You must enter an xobj file to be rendered\n")
	}

	obj, err := object.FromFile(*inFile)
	check(err)
	var img image.Image

	renderer.BackFaceRemoval = !*bf
	renderer.Clip = !*clip
	renderer.Wireframe = *wf
	renderer.ZBuffer = !*zbuf

	img, err = renderer.RenderObject(obj, (*size)*(*aa), (*size)*(*aa), *camera)
	check(err)

	if *aa != 1 {
		img = resize.Resize(uint(*size), uint(*size), img, resize.Lanczos3)
	}
	saveImage(img, *outFile)
}

func saveImage(img image.Image, file string) {

	f, err := os.Create(file)
	if err != nil {
		log.Fatal(err)
	}

	if err := png.Encode(f, img); err != nil {
		f.Close()
		log.Fatal(err)
	}

	if err := f.Close(); err != nil {
		log.Fatal(err)
	}
}
